from personne import Personne


class CompteSimple:
    '''Un compte simple a 2 attributs: un titulaire : string, un solde : int'''


    def __init__(self, titulaire, solde, num):
        '''Creer un copte simple'''
        self.titulaire = titulaire
        self.__solde = solde
        self.__numero = num

    @property 
    def solde(self):
        return self.__solde

    @property 
    def numero(self):
        return self.__numero

    def __str__(self):
        '''Affiche le compte simple'''
        return 'le solde du compte de ' + str(self.titulaire.nom) + ' est de ' + str(self.solde)

    def crediter(self, montant):
        '''Créditer un montant'''
        self.__solde += montant

    def debiter(self, montant):
        '''Débiter un montant'''
        self.__solde -= montant
    
