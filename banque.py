from compte_simple import *
from compte_courant import CompteCourant
from personne import Personne

class Banque:

    def __init__(self, prefixe):
        self.comptes = []
        self.__prefixe = prefixe
        

    @property
    def prefixe(self):
        return self.__prefixe

    def ouvrir_compte(self, titulaire, depot_initial, prefixe):
        num = prefixe + 1
        nouveau_compte = CompteSimple(titulaire, depot_initial, num)
        self.__prefixe = num
        self.comptes.append(nouveau_compte)
        return nouveau_compte
    
    def ouvrir_compte_courant(self, titulaire, depot_depart, prefixe):
        num = prefixe + 1
        nouveau_compte_courant = CompteCourant(titulaire, depot_depart, num)
        self.__prefixe = num
        self.comptes.append(nouveau_compte_courant)
        return nouveau_compte_courant

    def calculer_solde(self):
        somme_comptes = 0
        for compte in self.comptes:
            somme_comptes += compte.solde
        print(f'Le solde total des comptes de la banque est de {somme_comptes}€.')
        return somme_comptes

    def frais_prelevements(self):
        frais = 0
        for compte in self.comptes:
            compte.debiter(3)
            frais  += 3
        print(f'Le total des frais prélevé par la banque est de {frais}€.')
        return frais

    def editer_releve(self):  # checker isinstance
        print('Voici le relevé des comptes:')
        for compte in self.comptes:
            if isinstance(compte, CompteCourant):
                compte.editer_releve_compte()


def exemples():
    p2 = Personne('lex')
    b2 = Banque(1000)
    b3 = Banque(2000)
    b4 = Banque(3000)
    b5 = Banque(4000)
    print("les id des banques")
    print(b2.prefixe)
    print(b3.prefixe)
    print(b4.prefixe)
    c3 = b3.ouvrir_compte(p2, 600, b3.prefixe)
    c32 = b3.ouvrir_compte(p2, 600, b3.prefixe)
    c4 = b2.ouvrir_compte(p2, 600, b2.prefixe)
    c5 = b5.ouvrir_compte(p2, 600, b5.prefixe)
    c6 = b5.ouvrir_compte_courant(p2, 800, b5.prefixe)
    c7 = b5.ouvrir_compte_courant(p2, 800, b5.prefixe)
    c7.debiter(50)
    c7.crediter(300)
    c8 = b5.ouvrir_compte(p2, 600, b5.prefixe)
    print("les id des comptes simples")
    print(c3.numero)
    print(c32.numero)
    print(c4.numero)
    print(c5.numero)
    print(c8.numero)
    b5.editer_releve()
    print("les id des comptes courants")
    print(c6.numero)
    print(c7.numero)


if __name__ == "__main__":
    exemples()


