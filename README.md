
Comptes bancaires

Exercice 5 : Compte simple

Nous nous intéressons à un compte simple caractérisé par un solde exprimé en euros, positif ou
négatif, et son titulaire.Il est possible de créditer ce compte ou de le débiter d’un certain
montant.
5.1. Donner le diagramme de classe de la classe CompteSimple.
5.2. Écrire un programme de test de CompteSimple.
5.3. Écrire (puis tester) la classe CompteSimple.

Exercice 6 : Banque

En suivant la même démarche, définir une classe Banque qui gère des comptes. Elle offre les
opérations suivantes :
ouvrir un compte pour un client
calculer le total de l’argent géré par la banque (la somme des soldes de tous les comptes)
prélever des frais sur l’ensemble des comptes

Exercice 7 : Compte Simple (suite)

Modifier la classe CompteSimple pour que le solde ne soit plus accessible en écriture. Il ne faut
pas que cette modification ait un impact sur les programes qui utilisent CompteSimple, en
particulier Banque (et les programmes de test).

1. Nous simplifions le problème en considérant que tout compte a un et un seul titulaire correspondant à une personne
physique modélisée par une classe Personne.

Exercice 11 : Comptes courants

La banque gère aussi des comptes courants. Chaque compte courant conserve l’historique des
opérations qui le concernent (on se limite ici aux opérations de crédit et de débit). En plus des
opérations déjà disponibles sur un compte simple, on peut éditer un relevé de compte qui fait
apparaître toutes les opérations réalisées sur le compte et le solde final.
11.1. Compléter le diagramme UML pour faire apparaître la classe CompteCourant.
11.2. Écrire et tester la classe CompteCourant.

Exercice 12 : Compléter la banque

12.1. Ajouter une opération pour permettre d’ouvrir un compte courant.
12.2. Tester que les opérations existantes de la banque fonctionnent bien, que l’on ouvre des
comptes simples ou des comptes courants.
12.3. Ajouter une opération pour éditer le relevé des comptes gérés par la banque.

Exercice 13 : Numéros de comptes

Pour les différencier, chaque compte possède un numéro unique. On suppose que les numéros
sont des entiers et qu’ils sont attribués par ordre croissant en commençant à 10001. Les
numéros de compte sont donc 10001, 10002, 10003, etc. Dans la suite, nous envisageons deux
solutions pour attribuer les numéros de compte.
13.1. On souhaite que l’attribution du numéro de compte soit de la responsabilité des classes
CompteSimple et CompteCourant. Indiquer les modifications à apporter à l’application.
13.2. On suppose maintenant que c’est la banque qui gère et attribue les numéros de compte.
Indiquer les modifications à apporter à l’application.