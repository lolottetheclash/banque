import pytest
from banque import Banque
from personne import Personne
from compte_simple import CompteSimple
from compte_courant import CompteCourant


@pytest.fixture
def p1():
    return Personne("lolo")
    
@pytest.fixture
def b1():
    return Banque(1000)

def test__init__(p1):
    assert p1.nom == 'lolo'
    
def test__init__2(b1):
    assert b1.comptes == []

def test_ouvrir_compte(b1):
    b1.ouvrir_compte(p1, 300, 1000)
    assert len(b1.comptes) == 1
    assert type(b1.comptes[0]) == CompteSimple
    assert b1.comptes[0].solde == 300
    assert b1.comptes[0].titulaire == p1

def test_calculer_solde(b1):
    b1.ouvrir_compte(p1, 300, 1000)
    b1.ouvrir_compte(p1, 300, 1001)
    assert b1.calculer_solde() == 600
    assert b1.comptes[0].numero == 1001
    assert b1.comptes[1].numero == 1002

def test_verifier_numero_compte(b1):
    b1.ouvrir_compte(p1, 300, 1000)
    b1.ouvrir_compte(p1, 300, 1001)
    assert b1.comptes[0].numero == 1001
    assert b1.comptes[1].numero == 1002

def test_frais_prelevements(b1):
    b1.ouvrir_compte(p1, 300, 1000)
    b1.frais_prelevements()
    assert b1.comptes[0].solde == 297
    assert b1.comptes[0].numero == 1001


def test_ouvrir_compte_courant(b1):
    b1.ouvrir_compte_courant(p1, 500, 1000)
    assert len(b1.comptes) == 1
    assert type(b1.comptes[0]) == CompteCourant
    assert b1.comptes[0].solde == 500
    assert b1.comptes[0].titulaire == p1
