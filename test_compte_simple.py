import pytest
from compte_simple import CompteSimple

@pytest.fixture
def c1():
    return CompteSimple('Lolotte', 100, 1000)

@pytest.fixture
def c2():
    return CompteSimple('Nathie', 350, 2000)
    
def test__init__(c1):
    assert c1.titulaire == 'Lolotte'
    assert c1.solde == 100
    assert c1.numero == 1000


def test__init__2(c2):
    assert c2.titulaire == 'Nathie'
    assert c2.solde == 350
    assert c2.numero == 2000


def test_crediter(c1):
    c1.crediter(20)
    assert c1.solde == 120

def test_crediter2(c2):
    c2.crediter(50)
    assert c2.solde == 400

def test_debiter(c1):
    c1.debiter(30)
    assert c1.solde == 70

def test_debiter2(c2):
    c2.debiter(70)
    assert c2.solde == 280

