class Monnaie:

    def __init__(self, valeur, devise):
        self.valeur = valeur
        self.devise = devise

    def __repr__(self):
        return f'Monnaie({self.valeur}, {repr(self.devise)})'

    def __str__(self):
        return str(self.valeur) + ' ' + self.devise

    def _verifier_compatibles(self, autre_monnaie):
        if self.devise != autre_monnaie.devise:
            raise TypeError('Devises non compatibles')

    def ajouter(self, autre_monnaie):
        self._verifier_compatibles(autre_monnaie)
        self.valeur += autre_monnaie.valeur
    
        
    def retrancher(self, autre_monnaie):
        self._verifier_compatibles(autre_monnaie)
        self.valeur -= autre_monnaie.valeur

