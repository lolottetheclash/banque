from compte_simple import CompteSimple
from personne import Personne

class CompteCourant(CompteSimple): 

    def __init__(self, titulaire, solde, num):
        super().__init__(titulaire, solde, num)
        self.historique = list()

    def __str__(self):
        '''Affiche le compte courant'''
        return f"{super().__str__()}"

    def crediter(self, montant):
        '''Créditer un montant'''
        super().crediter(montant)
        self.historique.append(montant)

    def debiter(self, montant):
        super().debiter(montant)
        self.historique.append(-montant)

    def editer_releve_compte(self):
        """fait apparaître toutes les opérations réalisées sur le compte et le solde final"""
        #return self.historique, self.solde
        print(f"Voici l'historique des mouvements du compte: {self.historique}.")
        print(f'Voici le solde du compte: {self.solde}€.')


