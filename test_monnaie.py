import pytest
from monnaie import Monnaie 

@pytest.fixture
def m1():
    return Monnaie(5, 'euro')

@pytest.fixture
def m2():
    return Monnaie(7, 'euro')

@pytest.fixture
def m3():
    return Monnaie(9, 'dollar')

def test__init__(m1):
    assert m1.valeur == 5
    assert m1.devise == 'euro'

def test__init__2(m2):
    assert m2.valeur == 7
    assert m2.devise == 'euro'

def test__init__3(m3):
    assert m3.valeur == 9
    assert m3.devise == 'dollar'

def test_ajouter(m1, m2):
    m1.ajouter(m2)
    assert m1.valeur == 12

def test_retrancher(m1,m2):
    m1.retrancher(m2)
    assert m1.valeur == -2

def test_verifier_compatibles(m1,m3):
    with pytest.raises(TypeError):
        m1.ajouter(m3)

def test_verifier_compatibles2(m1,m3):
    with pytest.raises(TypeError):
        m1.retrancher(m3)
