class Personne:
    '''Creation d'une personne, nom : string'''

    def __init__(self, nom):
        self.nom = nom

    def __repr__(self):
        return "Personne({})".format(repr(self.nom))

#a = Personne("lauren")
#print(repr(a))
