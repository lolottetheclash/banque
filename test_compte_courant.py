import pytest
from compte_courant import CompteCourant
from compte_simple import CompteSimple
from banque import Banque

@pytest.fixture
def c1():
    return CompteCourant('lolo', 350, 3000)

@pytest.fixture
def c2():
    return CompteCourant('steph', 100, 4000)

@pytest.fixture
def b1():
    return Banque(1000)

def test__init__1(c1):
    assert c1.titulaire == 'lolo'
    assert c1.solde == 350
    assert c1.numero == 3000


def test__init__2(c2):
    assert c2.titulaire == 'steph'
    assert c2.solde == 100
    assert c2.numero == 4000


def test_crediter(c1):
    c1.crediter(300)
    assert c1.solde == 650    
    assert c1.historique == [300]

def test_crediter2(c2):
    c2.crediter(200)
    c2.crediter(100)
    assert c2.solde == 400   
    assert c2.historique == [200, 100]

def test_debiter(c1):
    c1.debiter(50)
    assert c1.solde == 300
    assert c1.historique == [-50]

def test_debiter2(c2):
    c2.debiter(20)
    c2.debiter(10)
    assert c2.solde == 70   
    assert c2.historique == [-20, -10]

def test_editer_releve(c1, b1):
    c1.debiter(50)
    c1.crediter(300)
    b1.frais_prelevements()
    assert [-50, 300, -3], 597 == c1.editer_releve()  # renvoie 2 valeurs: l'historique et le solde du compte

